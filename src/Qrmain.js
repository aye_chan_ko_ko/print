

import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import QrCodeInput from './QrCodeInput';
import {BrowserRouter,
  Switch,
  Route} from "react-router-dom";
import Card from './Card';

const Qrmain=()=>{
    return(
        <div>
            <React.StrictMode>
          {/* <App /> */}
          {/* <QrCode value='http://facebook.com'/> */}
          
          <BrowserRouter>
          <Switch>
            <Route exact path="/" component={QrCodeInput} />
            <Route path='/card' component={Card}/>
          </Switch>
          </BrowserRouter>
          
        </React.StrictMode>
        </div>
    )
}



// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
export default Qrmain;

